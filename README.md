# Zoomsense Plugin Trivia Leaderboard overlay

When running a trivia night, it can be nice to share the current leaderboard with participants.

This plugin lets you do that.
