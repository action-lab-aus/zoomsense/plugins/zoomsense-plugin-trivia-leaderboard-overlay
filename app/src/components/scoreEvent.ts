export type ScoreEvent = {
  amount: number;
  teamId: string;
};

export type ScoreEvents = { [scoreEventId: string]: ScoreEvent };
