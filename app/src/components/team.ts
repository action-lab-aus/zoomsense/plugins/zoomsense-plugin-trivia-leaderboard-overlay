export type User = {
  username: string;
  userId: string;
  userRole: string;
};

export type Team = {
  teamName: string;
  users: User[];
};

export type Teams = { [teamId: string]: Team };
