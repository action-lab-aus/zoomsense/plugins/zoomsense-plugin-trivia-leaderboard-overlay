import { ZoomSensePlugin } from "@zoomsense/zoomsense-firebase";
import { ScoreEvents } from "./scoreEvent";
import { Teams } from "./team";

declare module "@zoomsense/zoomsense-firebase" {
  interface ZoomSenseConfigCurrentStatePlugins {
    trivialeaderboardoverlay: {
      liveLeaderboardPaused: boolean;
    } & ZoomSensePlugin;
  }

  interface ZoomSenseDataPlugins {
    trivialeaderboardoverlay: {
      [meetingId: string]: {
        scoreEvents: ScoreEvents;
      };
    };
    teamPlugin: {
      [meetingId: string]: Teams;
    };
  }
}
