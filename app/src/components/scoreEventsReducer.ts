import { ScoreEvent } from "./scoreEvent";

export const scoreEventsReducer = (
  teamsScores: Record<string, number>,
  scoreEvent: ScoreEvent
) => {
  if (!teamsScores[scoreEvent.teamId]) {
    teamsScores[scoreEvent.teamId] = 0;
  }
  teamsScores[scoreEvent.teamId] =
    scoreEvent.amount + teamsScores[scoreEvent.teamId];
  return teamsScores;
};
