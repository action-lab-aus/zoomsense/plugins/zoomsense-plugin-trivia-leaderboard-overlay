# TODO

- [x] Render the team names in a list
- [x] Add the rest of the properties to the ScoreEvent type
- [x] Compute the score values for each team based on the score events
- [x] Render the score for each team
- [x] Sort the teams by score
- [x] Add dashboard displaying fixed list of teams.
- [ ] Add the ability to grant bonus scores to team.
- [ ] Display score of team.
